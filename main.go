package main

import (
	"fmt"
	. "github.com/eduardohitek/holidays"
	"time"
	"os"
	"strings"
	"github.com/joho/godotenv"
	"log"
)

func main()  {
	fmt.Println("Hello World!!!")
	feriados := HolidaysBR(2018)
	for k := range feriados {
		fmt.Println(k, feriados[k])
	}

	SetHolidaysFunction(HolidaysBR)
	date := time.Date(2018, time.September, 7, 0, 0, 0, 0, time.UTC)

	holidays := GetHolidays(date)
	for _, holiday := range holidays {
		fmt.Println(date, "is", holiday.Name)
	}

	fmt.Println("VAR: ",os.Getenv("MY_VAR"))
	for _, e := range os.Environ() {
        pair := strings.Split(e, "=")
        fmt.Println(pair[0],"=",pair[1])
    }

	err := godotenv.Load()
	if err != nil {
	  log.Fatal("Error loading .env file")
	}

	testeHitek := os.Getenv("TESTE_HITEK")
	fmt.Println(testeHitek)
}