# This file is a template, and might need editing before it works on your project.
FROM golang AS builder


WORKDIR /usr/src/app

COPY . .
#RUN go-wrapper download
RUN go get -d -v ./...
RUN go build -v

FROM buildpack-deps:jessie
ENV MY_VAR=%{MY_VAR}%

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/app/app .
COPY --from=builder /usr/src/app/.env .
CMD ["./app"]
